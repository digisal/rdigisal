# Initial version copied from https://gitlab.com/digisal/GSF1_metabolomics/-/blob/40d54a9/download.seek.R

#' Download file attached to SEEK record
#'
#' If authentication is required, it will be taken from these options in your `.Rprofile`:
#' \code{options(SEEKuser="<your SEEK username>", SEEKpass="<your SEEK password>")}
#'
#' @param url URL to the SEEK record, including `download`.
#' @param fname Destination file name. If NULL, use filename provided by server.
#'
#' @return Destination file name.
#' @export
#'
#' @examples
#' fname <- download_seek("https://fairdomhub.org/data_files/5659/download?version=1")
#' file.remove(fname)  # clean up
#' \dontrun{
#' download_seek("https://fairdomhub.org/data_files/1687/download?version=1")
#' }
download_seek <- function(url, fname=NULL) {

  SEEKuser <- getOption("SEEKuser")
  if (is.null(SEEKuser)) {
    # stop("Please put options(SEEKuser='<your username>', SEEKpass='<your password>') in your .Rprofile")
    auth <- NULL
  } else {
    SEEKpass <- getOption("SEEKpass")

    b64 <- base64enc::base64encode(charToRaw(paste0(SEEKuser, ":", SEEKpass)))
    auth <- paste("Basic", b64)
  }

  if (is.null(fname)) {
    r <- httr::GET(url, httr::add_headers(Authorization=auth))
    stopifnot(r$status_code==200)
    fname <- stringr::str_match(r$headers$`content-disposition`, 'attachment; filename=\\"(.*?)\\"')[2]
    writeBin(httr::content(r, "raw"), fname)
  } else {
    r <- httr::GET(url, httr::write_disk(fname, overwrite=FALSE), httr::add_headers(Authorization=auth))
    stopifnot(r$status_code==200)
  }
  invisible(fname)
}
