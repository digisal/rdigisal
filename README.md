## Installation

    devtools::install_gitlab("digisal/rdigisal")

## read_mbx()

Reads metabolomics results from MS-Omics.

## download_seek()

### Configuration

No configuration is needed to download files from public assets.
To download files from assets that require authentication, add the following to your `.Rprofile` (in your RStudio project folder or in your user folder), substituting your own username and password in the strings:

    options(SEEKuser="<your username>",
            SEEKpass="<your password>")


### Usage

See documentation and examples in `?download_seek`.
