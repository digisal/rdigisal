test_that("downloading without authentication works",
  {
    fname <- "UploadSigma.txt"
    expect_false(file.exists(fname))
    download_seek("https://fairdomhub.org/data_files/5659/download?version=1")
    expect_true(file.remove(fname))  # TRUE if file existed before removing
  })
